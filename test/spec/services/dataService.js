'use strict';
describe('Service: dataService', function () {

  var dataService;
  beforeEach(module('galleryApp'));
  beforeEach(inject(function (_dataService_) {
    dataService = _dataService_;
  }));

  it('should return correct list of slides not empty', inject(function ($rootScope) {
    dataService.getSlides().then(function(slides){
      expect(slides.length).toBeGreaterThan(0);
    });
    $rootScope.$digest();

  }));
});
