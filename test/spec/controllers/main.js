'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('galleryApp'));


  var MainCtrl,
    dataService,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope,_dataService_) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });

    dataService = _dataService_;

    MainCtrl.$inject = ['dataService'];
  }));

  it('should get a list from dataService', inject(function ($rootScope) {
    dataService.getSlides().then(function(slides){
      expect(slides.length).toBeGreaterThan(0);
    });
    $rootScope.$digest();
  }));

});
