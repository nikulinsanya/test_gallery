# gallery
Test application made by Sanya Nikulin
[Demo](http://nikulinsanya.pp.ua/gallery)

# used materials
 [Kickstart Your AngularJS Development with Yeoman, Grunt and Bower](http://www.sitepoint.com/kickstart-your-angularjs-development-with-yeoman-grunt-and-bower/)

 [UI Bootstrap](https://angular-ui.github.io/bootstrap/)

 [Full-Spectrum Testing with AngularJS and Karma](http://www.yearofmoo.com/2013/01/full-spectrum-testing-with-angularjs-and-karma.html)

 [Jasmine syntax](http://jasmine.github.io/2.3/introduction.html)

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.