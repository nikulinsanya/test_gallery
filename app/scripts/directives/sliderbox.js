'use strict';
angular.module('galleryApp')
  .directive('sliderBox',  function(){
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      templateUrl:'views/templates/directives/sliderbox.html',
      scope: {
        options: '='
      },
      link: function($scope){
        $scope.options = $scope.options || {};
        $scope.model = {
          text: $scope.options.text,
          title: $scope.options.title,
          price: $scope.options.price
        };

      }
    };
  });
