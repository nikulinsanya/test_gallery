'use strict';

/**
 * @ngdoc function
 * @name galleryApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the galleryApp
 */
angular.module('galleryApp')
  .controller('MainCtrl',['$scope','dataService', function ($scope, dataService) {
    $scope.model = {
      slides: [],
      sliderInterval : 3000,
      isTouchDevice : Modernizr.touch
    };

    dataService.getSlides().then(function(slides){
      $scope.model.slides = slides;
    });
  }]);
