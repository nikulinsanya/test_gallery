'use strict';
angular.module('galleryApp')
  .factory('dataService',['$q', function ($q) {
    var path = 'images/carousel/home',
      slides = [
      {
        image: path+ 1+'.jpg',
        text: 'Lorem ipsum bla-bla-bla1',
        price: 179,
        title: 'Africa'
      },
      {
        image: path+2+'.jpg',
        text: 'Lorem ipsum bla-bla-bla2',
        price: 99,
        title: 'Europe'
      },
      {
        image: path+3+'.jpg',
        text: 'Lorem ipsum bla-bla-bla4',
        price: 979,
        title: 'North Pole'
      },
      {
        image: path+4+'.jpg',
        text: 'Lorem ipsum bla-bla-bla5',
        price: 179,
        title: 'Ukraine'
      }
      ];

    return {
      getSlides: function(){
        var defer = $q.defer();
        defer.resolve(slides);
        return defer.promise;
      }
    };



  }]);
